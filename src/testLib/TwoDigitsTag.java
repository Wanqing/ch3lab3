package testLib;

import java.text.DecimalFormat;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

public class TwoDigitsTag implements Tag {
	private PageContext pageContext;
	private Tag parentTag;

	//initialization part
	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
	}

	public void setParent(Tag parentTag) {
		this.parentTag = parentTag;
	}

	public Tag getParent() {
		return this.parentTag;
	}

	//A String that holds the user attributes
	private String num;

	//The setter method that is called by the container
	public void setNum(String num) {
		this.num = num;
	}

	//actual tag handling code
	public int doStartTag() throws JspException {
		try {
			DecimalFormat twoDigits = new DecimalFormat("0.00");
			double d1 = Double.parseDouble(num);
			String s1 = twoDigits.format(d1);

			JspWriter out = pageContext.getOut();
			out.print(s1);
		}
		catch (Exception e) {
			throw new JspException("error");
		}
		return SKIP_BODY;
	}

	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	public void release()//clean up the resources (if any)
	{
	}
}
