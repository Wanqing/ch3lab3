<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="test" uri="/WEB-INF/testLib.tld" %>
<%@ page import="java.lang.Double" %>

<html><body>

<h3>Please enter Fahrenheit temperature : </h3><p>

<form> 
Temperature(F) : <input type="text" name="temperature"><br><br>
<input type="submit" value="Submit">
</form>

<%
    String temperature = request.getParameter("temperature");
    if(temperature != null && (!temperature.equals("")))
    {
         double tempF = Double.parseDouble(temperature);
         String tempC = "" + (tempF -32)*5.0/9.0;

%>
    <h3> <%= temperature %> degrees in Fahrenheit is converted to
         <test:twoDigits num="<%= tempC %>" />
         degrees in Celsius
    </h3>
    <%
    }
    %>
		  
</body></html>